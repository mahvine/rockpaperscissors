/**
 * Copyright 2017-present, Facebook, Inc. All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* eslint-disable camelcase */
/* eslint-disable max-len */

/*
 * BUTTONS
 *
 * Objects and methods that create objects that represent
 * buttons to be used in various UI elements.
 */

/**
 * Button for opening a specific list in a webview
 *
 * @param {string} gameUrl - URL for a specific game.
 * @param {string} buttonText - Text for the action button.
 * @returns {object} -
 *   Message to create a button pointing to the list in a webview.
 */
const openExistingGameButton = (gameUrl, buttonText = 'Open game') => {
  return {
    type: 'web_url',
    title: buttonText,
    url: gameUrl,
    messenger_extensions: true,
    webview_height_ratio: 'tall',
    webview_share_button: 'hide'
  };
};

/**
 * Button for opening a new list in a webview
 *
 * @param {string} apiUri - Hostname of the server.
 * @param {string=} buttonTitle - Button title.
 * @returns {object} -
 *   Message to create a button pointing to the new list form.
 */
const createGameButton = (apiUri, buttonTitle = 'Start a game') => {
  return {
    type: 'web_url',
    url: `${apiUri}/`,
    title: buttonTitle,
    webview_height_ratio: 'compact',
    messenger_extensions: true,
    webview_share_button: 'hide'
  };
};

/*
 * MESSAGES
 *
 * Objects and methods that create objects that represent
 * messages sent to Messenger users.
 */

/**
 * Message that welcomes the user to the bot
 *
 * @param {string} apiUri - Hostname of the server.
 * @returns {object} - Message with welcome text and a button to start a new list.
 */
const welcomeMessage = (apiUri) => {
  return {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'button',
        text: 'Challenge your friends to a game of 💪',
        buttons: [
          createGameButton(apiUri),
        ],
      },
    },
  };
};

/**
 * Helper to construct a URI for the desired list
 *
 * @param {string} apiUri -
 *   Base URI for the server.
 *   Because this moduele may be called from the front end, we need to pass it explicitely.
 * @param {int} gameId - The list ID.
 * @returns {string} - URI for the required list.
 */
const gameUrl = (apiUri, gameId) => `${apiUri}/games/${gameId}`;


/**
 * Message to configure the customized sharing menu in the webview
 *
 * @param {string} apiUri - Application basename
 * @param {string} gameId - The ID for game to be shared
 * @param {string} title - Title of the list
 * @param {string} buttonText - Text for the action button.
 * @returns {object} - Message to configure the customized sharing menu.
 */
const shareGameMessage = (apiUri, gameId, title, buttonText) => {
  const urlToList = gameUrl(apiUri, gameId);
  const gameNum = Math.floor(Math.random() * 6) +1;
  //
  return {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'generic',
        elements: [{
          title: title,
          image_url: `${apiUri}/media/game${gameNum}.gif`,
          subtitle: 'A game of chance that might change your life',
          default_action: {
            type: 'web_url',
            url: urlToList,
            messenger_extensions: true,
            webview_share_button: 'hide'
          },
          buttons: [openExistingGameButton(urlToList, buttonText)]
        }],
      },
    },
  };
};

export default {
  welcomeMessage,
  createGameButton,
  shareGameMessage,
};
