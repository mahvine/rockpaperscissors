



    /**
     * returns:
     * 0 if no one won 
     * 1 hand won 
     * 2 otherHand won
     * @param {String} hand 
     * @param {String} otherHand 
     */
    const compareHand = (hand, otherHand) => {
        hand = hand.toLowerCase();
        otherHand = otherHand.toLowerCase();
        if(hand == "scissor"){
            if(otherHand == "rock") return 2;
            if(otherHand == "paper") return 1;
            if(otherHand == "scissor") return 0;
        }
        
        if(hand == "rock"){
            if(otherHand == "rock") return 0;
            if(otherHand == "paper") return 2;
            if(otherHand == "scissor") return 1;
        }
        
        if(hand == "paper"){
            if(otherHand == "rock") return 1;
            if(otherHand == "paper") return 0;
            if(otherHand == "scissor") return 2;
        }
    }

export default {compareHand};