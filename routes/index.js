/**
 * Copyright 2017-present, Facebook, Inc. All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 */

// ===== MODULES ===============================================================
import express from 'express';
import games from './games';

const router = express.Router();

// GET home page
router.get('/', (req, res) => {
  res.render('./index', {pagetitle:'New Game 💪', gameId: 0, challenger: false, demo: process.env.DEMO});
});

router.get('/privacy', (req, res) => {
  res.render('./privacy', {demo: process.env.DEMO});
});

router.get('/games/:gameId', (req, res) => {
  res.render('./index', {pagetitle:'👊 🖐️ ✌️', gameId: req.params.gameId, challenger: true, demo: process.env.DEMO});
});

export default router;
