/**
 * Copyright 2017-present, Facebook, Inc. All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 */

// ===== MODULES ===============================================================
import express from 'express';

// ===== MESSENGER =============================================================
import userApi from '../messenger-api-helpers/user';


const router = express.Router();

router.get('/:fbId', (req, res) => {
  const {hostname} = req;
  const {DEMO, PORT, LOCAL} = process.env;
  const fbId = req.params.fbId;
  const socketAddress = (DEMO && LOCAL) ?
    `http://${hostname}:${PORT}` : `wss://${hostname}`;

    getUserDetails(fbId)
    .then((fbResponse) => {
        console.log(fbResponse);
        res.json(fbResponse);
    });

});



// Promise wrapper for Facebook UserApi.
const getUserDetails = (senderId) => {
    return new Promise((resolve, reject) => {
      userApi.getDetails(senderId, (err, {statusCode}, body) => {
        if (err) {
          console.log("Error:");
          console.log(err);
          return reject(err);
        } else if (statusCode !== 200) {
          console.log("Cannot fetch user data status:"+statusCode);
          console.log(body);
          return reject({
            statusCode,
            message: 'Unable to fetch user data for user',
            senderId,
          });
        }
  
        return resolve({
          name: body.first_name || body.last_name || senderId,
          profilePic: body.profile_pic,
          fbId: senderId,
        });
      });
    });
  };

  
export default router;
