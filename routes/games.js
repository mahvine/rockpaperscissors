/**
 * Copyright 2017-present, Facebook, Inc. All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 */

// ===== MODULES ===============================================================
import express from 'express';

// ===== DB ====================================================================
import Games from '../models/games';
import Users from '../models/users';
import RockPaperScissor from '../game/rockpaperscissor';

const router = express.Router();

router.get('/:gameId', (req, res) => {
  const {hostname} = req;
  const {DEMO, PORT, LOCAL} = process.env;
  const gameId = req.params.gameId;
  const socketAddress = (DEMO && LOCAL) ?
    `http://${hostname}:${PORT}` : `wss://${hostname}`;

  if (gameId === 'new') {
    res.send("new game")
  } else {
    Games.get(gameId)
      .then(game=>{
        console.log("Found game:"+gameId);
        res.json(game);
      })
      .catch(error=>{
        console.log("Cannot find game:"+gameId);
        res.sendStatus(404);
      });
  }
});


router.post('/', (req, res) => {
  const {hostname} = req;
  const {DEMO, PORT, LOCAL} = process.env;
  const socketAddress = (DEMO && LOCAL) ?
    `http://${hostname}:${PORT}` : `wss://${hostname}`;
    let hand1 = req.body.hand1;
    let hand2 = req.body.hand2;
    let hand3 = req.body.hand3;
    let fbUserId = req.body.fbUserId;
    let gameId = req.body.gameId;

    let fbUser =  {'fb_id':fbUserId};
    Users.findOrCreate(fbUser);//save the bastard
    console.log("New game request:");
    console.log(req.body);
    if(gameId){
      Games.updateOwnerHand(gameId, hand1, hand2, hand3).then(game=>{
        console.log("Update game hand:"+game.id+"|"+hand1+" "+hand2+" "+hand3+" "+fbUserId);
        res.json(game);
      });
    } else {
      Games.create(fbUserId, hand1, hand2, hand3).then(game=>{      
        console.log("New game:"+game.id+"|"+hand1+" "+hand2+" "+hand3+" "+fbUserId);
        res.json(game);
      });
    }
});


router.post('/:gameId/challenger', (req, res) => {
  const {hostname} = req;
  const {DEMO, PORT, LOCAL} = process.env;
  const gameId = req.params.gameId;
  const socketAddress = (DEMO && LOCAL) ?
    `http://${hostname}:${PORT}` : `wss://${hostname}`;
    let otherHand1 = req.body.hand1;
    let otherHand2 = req.body.hand2;
    let otherHand3 = req.body.hand3;
    let otherFbUserId = req.body.fbUserId;
    let otherFbUser =  {'fb_id':otherFbUserId};
    Users.findOrCreate(otherFbUser);//save the bastard
    Games.get(gameId).then(game=>{
      console.log("Challenges game:"+game.id+"|"+otherHand1+" "+otherHand2+" "+otherHand3+" "+otherFbUserId);
      const hand1 = game.ownerHand1;
      const hand2 = game.ownerHand2;
      const hand3 = game.ownerHand3;
      let score = 0;
      let challengerScore = 0;

      const round1 = RockPaperScissor.compareHand(hand1, otherHand1);
      const round2 = RockPaperScissor.compareHand(hand2, otherHand2);
      const round3 = RockPaperScissor.compareHand(hand3, otherHand3);

      if (round1 == 1) {
        score++;
      } else if (round1 == 2) {
        challengerScore++;
      }
      
      if (round2 == 1) {
        score++;
      } else if (round2 == 2) {
        challengerScore++;
      }
      
      if (round3 == 1) {
        score++;
      } else if (round3 == 2) {
        challengerScore++;
      }

      let winnerId = null;
      if(score > challengerScore) {
        winnerId = game.ownerFbId;
      }
      
      if(challengerScore > score) {
        winnerId = otherFbUserId;
      }
      console.log('Challenger score:'+challengerScore+' vs score:'+score)

      Games.updateChallenger(game.id, otherFbUserId, otherHand1, otherHand2, otherHand3, winnerId);
      console.log('winner:'+winnerId);
      res.json({winnerFbId:winnerId});
    });
});


export default router;
