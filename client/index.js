import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App.jsx';
import Oops from './components/Oops.jsx';

console.log("JOSHVIN DOMINGO was here");

/*
 * Function for attaching the application when MessengerExtensions has loaded
 */
window.attachApp = (viewerId, gameId, challenger, threadType) => {
    const apiUri = `https://${window.location.hostname}`;
    
    let app;
    if (viewerId) {
      app = (
        // The main show
        <App
          viewerId={viewerId}
          gameId={gameId}
          apiUri={apiUri}
          challenger={challenger}
          threadType={threadType}
        />
      );
    } else {
      /**
       * MessengerExtensions are only available on iOS and Android,
       * so show an error page if MessengerExtensions was unable to start
       */
      app = <Oops />;
    }
  
    ReactDOM.render(app, document.getElementById('content'));
  };
  

