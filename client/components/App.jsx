import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import logo from './logo.svg';
import { Button, Flex } from 'react-weui';

import 'weui';
import 'react-weui/build/packages/react-weui.css';
import './App.css';
import Hand from './hands/Hand.jsx';
import Result from './result.jsx';
import Invite from './invite.jsx';
import LoadingScreen from './loading_screen.jsx';

export default class App extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      isChallenger:false,
      gameId: props.gameId,
      viewerId: props.viewerId,
      counter:0,
      hand:['','',''],
      challengerHand:['','',''],
      showHandSelection: true,
      results: false,
      owner:{},
      challenger:{},
      viewer:{},
      newGameTitles:['A new duel request 👊','New game of Hands 🖐️🖐️','You are being challenged 💪','Are you up for a game? ✌️'],
      showApp:false
    };
  }
  
  //great way to initialize here
  componentWillMount() {
    console.log('Viewer ID:'+this.props.viewerId);
    console.log(this.state);
    let showApp = true;
    if(this.state.gameId>0){
      fetch('/api/games/'+this.state.gameId)
        .then(response =>{
          response.json().then(json=>{
            console.log("Game details:");
            console.log(json);
            let hand = this.state.hand;
            hand[0] = json.ownerHand1;
            hand[1] = json.ownerHand2;
            hand[2] = json.ownerHand3;

            let challengerHand = this.state.challengerHand;
            challengerHand[0] = json.challengerHand1;
            challengerHand[1] = json.challengerHand2;
            challengerHand[2] = json.challengerHand3;

            let showHandSelection = true;
            const viewerId = this.state.viewerId;
            const ownerFbId = json.ownerFbId;
            let challengerFbId = null;
            let isChallenger;
            if(viewerId == json.ownerFbId){
              showHandSelection = false;
              isChallenger = false;
            } else {
              isChallenger = true;
            }
            if(json.challengerHand3){
              showHandSelection = false;
              if(json.challengerFbId){
                challengerFbId = json.challengerFbId;
              }
            }
            this.setState({challengerHand, hand, showHandSelection, isChallenger, ownerFbId, challengerFbId, showApp});
            
            this._fetchChallenger();
            this._fetchOwner();
            this._fetchViewer();
            
            
            if(json.challengerHand3){
              //Show result
              this._showResult();
            }
          });
        })
    } else {
      console.log("Requesting new game...");
      //CREATE new game
      fetch('/api/games', {
          method: 'post',
          headers: new Headers({'Content-Type': 'application/json'}),
          body: JSON.stringify({fbUserId:this.props.viewerId})
          })
        .then(
          response => {
            if (response.status !== 200) {
              console.log('Looks like there was a problem. Status Code: ' +
                response.status);
              return;
            }
            console.log("New game response:");
            response.json().then(json=>{
              console.log(json);
              this.setState({gameId:json.id,isChallenger:false, showApp});
            });
          }
        )
        .catch(function(err) {
          console.log('Fetch Error :-S', err);
        });
    }
    
  }

  render() {
    
    const {showHandSelection,isChallenger,results,ownerFbId, gameId, viewerId, showApp, challengerFbId, selectionState} = this.state;
    const {apiUri, threadType} = this.props;
    let myHands = this.state.hand;
    let otherHands = this.state.challengerHand;
    let player1Name = this.state.owner.name;
    let player1Pic = this.state.owner.profilePic;
    let player2Name = this.state.challenger.name;
    let player2Pic = this.state.challenger.profilePic;
    let isPlayer = (challengerFbId === viewerId || ownerFbId === viewerId);

    if(isChallenger && challengerFbId === viewerId){
      myHands = this.state.challengerHand;
      otherHands = this.state.hand;
      player1Name = this.state.challenger.name;
      player1Pic = this.state.challenger.profilePic;
      player2Name = this.state.owner.name;
      player2Pic = this.state.owner.profilePic;
    }

    
    const title = this.state.newGameTitles[Math.floor(Math.random() * 4)];
    let sharingMode = 'broadcast';
    let buttonText = 'Challenge a friend 😏';

    let handSelection;
    let challengerHandSelection;
    let ownerHandSelection;
    let inviteButton;
     

    if(showHandSelection == true){
      handSelection = (<div className={['selection', selectionState].join(' ')}>
      <p>Select your hands!</p>
        <Flex>
          <Hand className={selectionState} handClicked={this._handClicked.bind(this)} sign='rock'/>
          <Hand className={selectionState} handClicked={this._handClicked.bind(this)} sign='paper'/>
          <Hand className={selectionState} handClicked={this._handClicked.bind(this)} sign='scissor'/>
        </Flex>
      </div>);
    } else{
      if(challengerFbId === undefined){
        inviteButton = (<Invite
          title={title}
          apiUri={apiUri}
          gameId={gameId}
          sharingMode={sharingMode}
          buttonText={buttonText}
        />)
      }
    }

    if(isChallenger==true && results == false){
      console.log("He is a Challenger")
      challengerHandSelection = (
        <Flex className="selected">
          <p>Your selection</p>
          <div className="circle">
            <Hand className={this.state.challengerHand[0]?'show':''} sign={this.state.challengerHand[0]}/>
          </div>
          <div className="circle">
            <Hand className={this.state.challengerHand[1]?'show':''} sign={this.state.challengerHand[1]}/>
          </div>
          <div className="circle">
            <Hand className={this.state.challengerHand[2]?'show':''} sign={this.state.challengerHand[2]}/>
          </div>
        </Flex>
      );
    }

    if(isChallenger==false && results == false){
      console.log("Not challenger")
      ownerHandSelection = (
        <Flex className="selected">
          <p>Your selection</p>
          <div className="circle">
              <Hand sign={this.state.hand[0]}/>
          </div>
          <div className="circle">
              <Hand sign={this.state.hand[1]}/>
          </div>
          <div className="circle">
              <Hand sign={this.state.hand[2]}/>
          </div>
        </Flex>
      );
    }



    let page = <LoadingScreen key='load' />;
    
    
    // Show a loading screen until app is ready
    if(showApp==true){
      page = (
        <div>
          {handSelection}
          {challengerHandSelection}
          {ownerHandSelection}
          {inviteButton}
        </div>);
    }
    

    return (
      
      <div className="App">
      
        <ReactCSSTransitionGroup
            transitionName='page'
            transitionEnterTimeout={100}
            transitionLeaveTimeout={100}
          >
          {page}
          {(results==true && this.state.viewer.name!==undefined && this.state.challenger.name!==undefined && this.state.owner.name!==undefined)&&
            <Result ref="resultComponent" 
              otherHands={otherHands} 
              myHands={myHands}
              player1Name={player1Name}
              player2Name={player2Name}
              player1Pic={player1Pic}
              player2Pic={player2Pic}
              isPlayer={isPlayer}/>
          }
        </ReactCSSTransitionGroup>
      </div>
    );
  }

  _handClicked(sign){
    console.log("THIS IS from App:"+sign+" gameId:"+this.state.gameId);
    const counter = this.state.counter;
    //update UI
    if(this.state.isChallenger){
      let challengerHand = this.state.challengerHand;
      challengerHand[counter] = sign;
      this.setState({challengerHand, counter: this.state.counter+1, });
    } else {
      let hand = this.state.hand;
      hand[counter] = sign;
      this.setState({hand:hand, counter: this.state.counter+1});
    }

    //send to server
    if(counter == 2){
      
      this.setState({selectionState:'fold-down'});
      setTimeout(()=>{
        this.setState({showHandSelection:counter<2});
      },1000);

      if(this.state.isChallenger){
        this._updateChallengerHand();
      } else {
        this._updateHand();
      }

      //Show result
      if(this.state.hand[2]!=='' && this.state.challengerHand[2]!==''){
        this._showResult();
      }
    }
  }

  _updateHand(){
    const gameId = this.state.gameId;
    const fbUserId = this.state.viewerId;
    const hand1 = this.state.hand[0];
    const hand2 = this.state.hand[1];
    const hand3 = this.state.hand[2];
    //for Creator
    fetch('/api/games', {
      method: 'post',
      headers: new Headers({'Content-Type': 'application/json'}),
      body: JSON.stringify({gameId,fbUserId, hand1,hand2,hand3})
      })
    .then(
      response => {
        if (response.status !== 200) {
          console.log('Looks like there was a problem. Status Code: ' +
            response.status);
          return;
        }
        console.log("Response:");
        response.json().then(json=>{
          console.log(json);
          this.setState({gameId:json.id});
        });
      }
    )
    .catch(function(err) {
      console.log('Fetch Error :-S', err);
    });
  }

  _updateChallengerHand(){
    const gameId = this.state.gameId;
    const fbUserId = this.state.viewerId;
    const hand1 = this.state.challengerHand[0];
    const hand2 = this.state.challengerHand[1];
    const hand3 = this.state.challengerHand[2];
    
    this.setState({challenger:this.state.viewer,challengerFbId:fbUserId, showApp:false});
    //for Challenger
    fetch('/api/games/'+gameId+'/challenger', {
      method: 'post',
      headers: new Headers({'Content-Type': 'application/json'}),
      body: JSON.stringify({fbUserId, hand1,hand2,hand3})
      })
    .then(
      response => {
        if (response.status !== 200) {
          console.log('Looks like there was a problem. Status Code: ' +
            response.status);
          return;
        }
        console.log("Response:");
        response.json().then(json=>{
          console.log(json);
        });
        this.setState({showApp:true});
      }
    )
    .catch(function(err) {
      console.log('Fetch Error :-S', err);
    });
  }


  _showResult() {
    this.setState({results:true});
    let myinterval = setInterval(()=>{
      if(this.refs.resultComponent){
        this.refs.resultComponent._startAnimationLoop();
        clearInterval(myinterval);
      }
    },1000);
  }

  
  _fetchViewer() {
    if(this.state.viewerId)
    fetch('/api/users/'+this.state.viewerId)
        .then(response =>{
          response.json().then(json=>{
            console.log("Viewer details:");
            console.log(json);
            this.setState({viewer:json});
          });
        });
  }
  
  _fetchOwner() {
    if(this.state.ownerFbId)
    fetch('/api/users/'+this.state.ownerFbId)
        .then(response =>{
          response.json().then(json=>{
            console.log("Owner details:");
            console.log(json);
            this.setState({owner:json});
          });
        });
  }
  
  _fetchChallenger() {
    if(this.state.challengerFbId)
    fetch('/api/users/'+this.state.challengerFbId)
        .then(response =>{
          response.json().then(json=>{
            console.log("Challenger details:");
            console.log(json);
            this.setState({challenger:json});
          });
        });
  }

}
