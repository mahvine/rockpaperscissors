/**
 * Copyright 2017-present, Facebook, Inc. All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* eslint-disable react/react-in-jsx-scope */

/* ----------  External Libraries  ---------- */
import React, { Component } from 'react';
import {Button} from 'react-weui';

/* ----------  Messenger Helpers  ---------- */

import messages from '../../messenger-api-helpers/messages';

/*
 * Button to invite firends by invoking the share menu
 */

export default class Invite extends Component {
  
  constructor(props) {
    super(props);

  }


  render(){
    const iconClassName = this.props.sharingMode === 'broadcast' ? 'share' : 'send';
    const {buttonText} = this.props;
    return (
      <div id='invite'>
        <Button onClick={this._shareGame.bind(this)}>
          <span className={`invite-icon ${iconClassName}`} />
          {buttonText}
        </Button>
      </div>
    );
  }
  
  componentDidMount(){
    console.log("Invite props did mount");
    console.log(this.props);
  }

  _shareGame(){
    const props = this.props;
    
    window.MessengerExtensions.beginShareFlow((response)=>{
        if (response.is_sent) {
          window.MessengerExtensions.requestCloseBrowser(null, null);
        }
      }, (errorCode, errorMessage) => {
        console.error({errorCode, errorMessage});
      },
      messages.shareGameMessage(props.apiUri, props.gameId, props.title),
      props.sharingMode);
  }







}


