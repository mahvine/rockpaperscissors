import React, { Component } from 'react';
import { Flex } from 'react-weui';
import Hand from './hands/Hand';

import 'weui';
import 'react-weui/build/packages/react-weui.css';
import './result.css'

import RockPaperScissor from '../../game/rockpaperscissor';

export default class Result extends Component {
  
  constructor(props) {
    super(props);
    this._timer;
    this._counter = 0;
    this.state = {
      otherHands:['','',''],
      myWins:[''],
      otherWins:[''],
      myScore:0,
      otherScore:0,
      player1Name: props.player1Name,
      player1Pic: props.player1Pic,
      player2Name: props.player2Name,
      player2Pic: props.player2Pic,
      realOtherHands:props.otherHands,
      myHands:props.myHands,
      isPlayer:props.isPlayer
    };
    console.log('Result state:');
    console.log(this.state);
    this._loseImages = ['https://media.giphy.com/media/EU5ox7Mve7FS0/200w_d.gif','https://media.giphy.com/media/11xqf2W6yM0bLO/200w_d.gif','https://media.giphy.com/media/VILJHh5AodVIs/200w_d.gif','https://media.giphy.com/media/9Y5BbDSkSTiY8/200w_d.gif'];
    this._winImages = ['https://media.giphy.com/media/2gtoSIzdrSMFO/200w_d.gif','https://media.giphy.com/media/26gsf3YejgPfbLvig/200w_d.gif','https://media.giphy.com/media/xT0GqssRweIhlz209i/200w_d.gif','https://media.giphy.com/media/26BkNituin1dca6GI/200w_d.gif'];
    this._drawImages = ['https://media.giphy.com/media/26ufkv6PUIzb2ZQre/200w_d.gif','https://media.giphy.com/media/3orifeagG1UwX4DeO4/200w_d.gif','https://media.giphy.com/media/26gsf0Ys35me32EkE/200w_d.gif']
  }
  
  componentWillMount() {
    
    const loseNum = Math.floor(Math.random() * this._loseImages.length);
    const winNum = Math.floor(Math.random() * this._winImages.length);
    const drawNum = Math.floor(Math.random() * this._drawImages.length);
    this.setState({winImg:this._winImages[winNum],loseImg:this._loseImages[loseNum],drawImg:this._drawImages[drawNum]});
  }

  render() {
    let resultText = '';
    if(this.state.win){
      if(this.state.isPlayer){
        resultText = 'You win!';
      } else {
        resultText = this.state.player1Name+' win!';
      }
    }
    if(this.state.lose){
      if(this.state.isPlayer){
        resultText = 'You lose!';
      } else {
        resultText = this.state.player2Name+' win!';
      }
    }
    if(this.state.draw){
      resultText = 'Draw!';
    }

    return (
      <div className="Result">
        <Flex className="topPlayer">
          <div>{this.state.player2Name}</div>
          <div className="otherhands">
          <div className={['result-circle', this.state.otherWins[0]].join(' ')}>
                <Hand sign={this.state.otherHands[0]}/>
            </div>
            <div className={['result-circle', this.state.otherWins[1]].join(' ')}>
              <Hand sign={this.state.otherHands[1]}/>
            </div>
            <div className={['result-circle', this.state.otherWins[2]].join(' ')}>
              <Hand sign={this.state.otherHands[2]}/>
            </div>
          </div>
        </Flex>
        <div className="clashArea">
          <div className="otherHand">
            <Hand sign={this.state.otherHand}/>
          </div>
          <div className="myHand">
            <Hand sign={this.state.myHand}/>
          </div>
          <div className='result-div'>
            <p>{resultText}</p>
            <img className={this.state.win?'show':''} src={this.state.winImg}/>
            <img className={this.state.lose?'show':''} src={this.state.loseImg}/>
            <img className={this.state.draw?'show':''} src={this.state.drawImg}/>
          </div>
        </div>
        <Flex className="bottomPlayer">
          <div className="myhands">
            <div className={['result-circle', this.state.myWins[0]].join(' ')}>
              <Hand sign={this.state.myHands[0]}/>
            </div>
            <div className={['result-circle', this.state.myWins[1]].join(' ')}>
              <Hand sign={this.state.myHands[1]}/>
            </div>
            <div className={['result-circle', this.state.myWins[2]].join(' ')}>
              <Hand sign={this.state.myHands[2]}/>
            </div>
          </div>
          <div>{this.state.player1Name}</div>
        </Flex>
      </div>
    );
  }

  _startAnimationLoop(){
    //Animationloop here
    const _timer = window.setInterval(()=>{

      const myHand = this.state.myHands[this._counter]; 
      const otherHand = this.state.realOtherHands[this._counter];
      let otherHands = this.state.otherHands;
      otherHands[this._counter] = otherHand;
      let myWins = this.state.myWins;
      let otherWins = this.state.otherWins;
      const result = RockPaperScissor.compareHand(myHand,otherHand);

      let myScore = this.state.myScore;
      let otherScore = this.state.otherScore;
      if(result == 1){
        myWins[this._counter] = 'win';
        otherWins[this._counter] = 'lose';
        myScore++;
      }
      if(result == 2){
        myWins[this._counter] = 'lose';
        otherWins[this._counter] = 'win';
        otherScore++;
      }
      if(result == 0){
        myWins[this._counter] = 'lose';
        otherWins[this._counter] = 'lose';
      }

      this.setState({myHand, otherHand, otherHands, myWins, otherWins, myScore, otherScore});

      setTimeout(()=>{
        this.setState({myHand:'',otherHand:''});
      },1300);

      this._counter++;
      
      if(this._counter ==3){
        clearInterval(_timer);
        //Show winner
        setTimeout(()=>{
          if(this.state.myScore < this.state.otherScore){
            this.setState({lose:true});
          }
          if(this.state.otherScore < this.state.myScore){
            this.setState({win:true});
          }
          if(this.state.otherScore == this.state.myScore){
            this.setState({draw:true});
          }
        },2000)
      }
      

    },2000);
  }
}