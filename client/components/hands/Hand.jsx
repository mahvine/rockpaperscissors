import React, { Component } from 'react';
import rock from './rock-hand.png';
import paper from './paper-hand.png';
import scissor from './scissor-hand.png';
import placeholder from './placeholder-hand.png';
import './Hand.css';

class Hand extends Component {
  constructor(){
    super();
    this.state = {sign: ''};
  }

  componentWillMount() {
    this.setState({sign:this.props.sign});
  }

  render() {
    let imgConst = null;
    let active;
    if(this.props.sign == 'rock') {
      imgConst = rock;
      active='active';
    } else if(this.props.sign == 'paper') {
      imgConst = paper;
      active='active';
    } else if(this.props.sign == 'scissor') {
      imgConst = scissor;
      active='active';
    } else {
      imgConst = placeholder;
    }

    return (
      <div className={['Hand', active].join(' ')} onClick={this._handleClick.bind(this)}>
          <img src={imgConst}/> 
      </div>
    );
  }

  _handleClick(event){
    event.preventDefault();
    if(this.props.handClicked)
    this.props.handClicked(this.state.sign);
  }

}

export default Hand; 