
exports.up = function(knex, Promise) {
    
  return Promise.all([
    knex.schema.createTable('users', function (table) {
        table.increments();
        table.bigInteger('fb_id').unique().notNullable();
    }),
    
    knex.schema.createTable('games', (table) => {
        table.increments();
        table.bigInteger('owner_fb_id').references('users.fb_id').notNullable();
        table.bigInteger('challenger_fb_id').references('users.fb_id');
        table.bigInteger('winner_fb_id').references('users.fb_id');
        table.dateTime('start_time');
        table.dateTime('end_time');
  
        table.string('owner_hand_1');
        table.string('owner_hand_2');
        table.string('owner_hand_3');
        table.string('challenger_hand_1');
        table.string('challenger_hand_2');
        table.string('challenger_hand_3');
    })

  ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists('users'),
        knex.schema.dropTableIfExists('games')
    ]);
};
