
const GAMES = [
    {id:1,owner_fb_id: 1,challenger_fb_id:2, winner_fb_id:1,owner_hand_1:'Rock',owner_hand_2:'Rock',owner_hand_3:'Scissors',challenger_hand_1:'Paper',challenger_hand_2:'Scissors',challenger_hand_3:'Paper'},
    {id:2,owner_fb_id: 1,owner_hand_1:'Rock',owner_hand_2:'Rock',owner_hand_3:'Scissors'}
  ];
  
  // Constants for placeholder User data for seed files.
  const USERS = [
    {fb_id: 1},
    {fb_id: 2},
    {fb_id: 3},
    {fb_id: 4},
  ];
  
  
  module.exports = {GAMES, USERS};
  