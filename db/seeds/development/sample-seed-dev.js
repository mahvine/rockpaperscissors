
const {GAMES, USERS} =
require('../sample-seed-helper');

/**
* Dev ENV Seed File - When run with `knex seed:run`, populates
*                     database with placeholder data.
*/
exports.seed = (knex, Promise) =>
Promise.all([
  knex('games').del(),
  knex('users').del(),
]).then(() =>
  Promise.all([
    knex('users').insert(USERS, 'id'),
    knex('games').insert(GAMES, 'id'),
  ])
);
