
// ===== DB ====================================================================
import Knex  from '../db/knex';

// ===== UTIL ==================================================================
import {camelCaseKeys} from './util';

const Games = () => Knex('games');
const Users = () => Knex('users');

/**
 * get - Gets the Game with the given ID.
 * @param   {Number} gameId - The ID of the Game to return.
 * @returns {Object} Game - The matched Game.
 */
const get = (gameId) =>
  Games()
    .where('id', parseInt(gameId, 10))
    .first()
    .then(camelCaseKeys);

/**
 * create - Creates a new game from a user with the a new hands.
 * @param   {Number} fbId - owner of the game.
 * @param   {String} hand1 - first hand of the game.
 * @param   {String} hand2 - second hand of the game.
 * @param   {String} hand3 - third hand of the game.
 * @returns {Object} game - The newly created game.
 */
const create = (fbId, hand1, hand2, hand3) => {
    return Games()
      .insert({owner_fb_id:fbId, owner_hand_1:hand1, owner_hand_2:hand2, owner_hand_3:hand3,}, 'id').then(get);
}

/**
 * update - Updates the remaining info of the game.
 * @param   {Number} gameId - Id of game to be updated.
 * @param   {Number} fbId - challenger of the game.
 * @param   {String} hand1 - first hand of the game.
 * @param   {String} hand2 - second hand of the game.
 * @param   {String} hand3 - third hand of the game.
 * @returns {Object} game - The newly created game.
 */
const updateOwnerHand = (gameId, hand1, hand2, hand3) =>{
  return Games().where('id', parseInt(gameId, 10)).update({
      owner_hand_1: hand1,
      owner_hand_2: hand2,
      owner_hand_3: hand3,
    }, 'id').then((gameId) => get(gameId));
}
/**
 * update - Updates the remaining info of the game.
 * @param   {Number} gameId - Id of game to be updated.
 * @param   {Number} fbId - challenger of the game.
 * @param   {String} otherHand1 - first hand of the game.
 * @param   {String} otherHand2 - second hand of the game.
 * @param   {String} otherHand3 - third hand of the game.
 * @param   {String} winner - third hand of the game.
 * @returns {Object} game - The newly created game.
 */
const updateChallenger = (gameId, challengerFbId, otherHand1, otherHand2, otherHand3, winnerFbId) =>{
  return Games().where('id', parseInt(gameId, 10)).update({
        challenger_fb_id: challengerFbId,
        challenger_hand_1: otherHand1,
        challenger_hand_2: otherHand2,
        challenger_hand_3: otherHand3,
        winner_fb_id: winnerFbId
      }, 'id').then((gameId) => get(gameId));
}


export default {
  create,
  get,
  updateOwnerHand,
  updateChallenger
};
